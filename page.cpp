// 
// 
// 

#include "page.h"

page::page(Adafruit_GFX *gfx, getString f, buttonHandler bh):gfx(gfx), getStringCallback(f), buttonHandlerCallback(bh)
{
	init();
}

void page::init()
{
	controlsHead = 0;
	controlsTail = MAX_CONTROLS - 1;
	numControls = 0;
	controlFocus = 0;
}

bool page::addControl(byte x, byte y, byte l, controlTypes t, void* d, valueTypes v, byte c = 0)
{
	controls[controlsHead].x = x;
	controls[controlsHead].y = y;
	controls[controlsHead].labelId = l;

	controls[controlsHead].labelWidth = byte((*getStringCallback)(l).length() * 6);
	controls[controlsHead].type = t;
	controls[controlsHead].code = c;
	controls[controlsHead].vt = v;

	switch (t) {
		case controlTypes::INPUT_FIELD_INT:
		case controlTypes::INPUT_FIELD_DOUBLE:
		case controlTypes::LABEL_FIELD:
			controls[controlsHead].w = 50;
			controls[controlsHead].h = 12;
			break;
		case controlTypes::BUTTON:
			controls[controlsHead].w = byte(max(controls[controlsHead].labelWidth + 10, 50));
			controls[controlsHead].h = 13;
			break;
		case controlTypes::BIG_BUTTON:
			controls[controlsHead].w = byte(gfx->width() - (controls[controlsHead].x * 2));
			controls[controlsHead].h = 20;
			controls[controlsHead].labelWidth = byte(controls[controlsHead].labelWidth * 2);
			break;
	}

	if (t != controlTypes::BUTTON && t != controlTypes::BIG_BUTTON) {
		switch (v) {
			case valueTypes::P_BYTE:
				controls[controlsHead].vb = (byte*)d;
				break;
			case valueTypes::P_INT:
				controls[controlsHead].vi = (int*)d;
				break;
			case valueTypes::P_WORD:
				controls[controlsHead].vw = (word*)d;
				break;
			case valueTypes::P_LONG:
				controls[controlsHead].vl = (long*)d;
				break;
			case valueTypes::P_NONE:
				break;
		}
	}

	controlsHead = (controlsHead + 1) % MAX_CONTROLS;

	numControls++;

	return true;
}

bool page::isFull()
{
	return (controlsHead == controlsTail);
}

bool page::isEmpty()
{
	return (numControls == 0);
}

byte page::getNumControls()
{
	return numControls;
}

void page::RenderPage()
{
	gfx->fillScreen(ST7735_BLACK);
	gfx->setTextColor(ST7735_GREEN);

	for (byte i = 0; i < numControls; i++) {
		RenderFrame(i);
		RenderContent(i);
	}
}

void page::RenderFrame(byte idx)
{
	int color = ST7735_RED;

	switch (controls[idx].type) {
		case controlTypes::INPUT_FIELD_INT:
		case controlTypes::INPUT_FIELD_DOUBLE:
		case controlTypes::LABEL_FIELD:
			if (idx == controlFocus) {
				if (controlEditing) {
					color = ST7735_GREEN;
				}
				else
				{
					color = ST7735_YELLOW;
				}
			}

			break;
		case controlTypes::BUTTON:
		case controlTypes::BIG_BUTTON:
			
			color = ST7735_BLUE;
			if (idx == controlFocus) {
					color = ST7735_CYAN;
			}

			break;
	}

	gfx->drawRect(controls[idx].x, controls[idx].y, controls[idx].w, controls[idx].h, color);
}

void page::RenderContent(byte idx)
{
	int color = ST7735_BLACK;
	int fcolor = ST7735_GREEN;

	if (controls[idx].type == controlTypes::BUTTON || controls[idx].type == controlTypes::BIG_BUTTON) {
		color = ST7735_DEEPBLUE;
		fcolor = ST7735_WHITE;
	}
	
	// clean the inside of the rectangle
	gfx->fillRect(controls[idx].x + 1, controls[idx].y + 1, controls[idx].w - 2, controls[idx].h - 2, color);
	gfx->setTextColor(fcolor);

	switch (controls[idx].type) {
		case controlTypes::INPUT_FIELD_INT:
		case controlTypes::INPUT_FIELD_DOUBLE:
		case controlTypes::LABEL_FIELD:
			gfx->setCursor(controls[idx].x + 3, controls[idx].y + 3);
			PrintValue(idx);
			gfx->setCursor(controls[idx].x - controls[idx].labelWidth - 2, controls[idx].y + 3);
			gfx->print((*getStringCallback)(controls[idx].labelId));

			if (controls[idx].type == controlTypes::INPUT_FIELD_INT || controls[idx].type == controlTypes::INPUT_FIELD_DOUBLE) {
				switch (AdjustMultiplier) {
				case 10:
					gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 2, 4, ST7735_YELLOW);
					break;
				case 100:
					gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 2, 4, ST7735_YELLOW);
					gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 6, 4, ST7735_YELLOW);
					break;
				case 1000:
					gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 2, 4, ST7735_YELLOW);
					gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 6, 4, ST7735_YELLOW);
					gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 10, 4, ST7735_YELLOW);
					break;
				}
			}
			break;
		case controlTypes::BUTTON:
			gfx->setCursor((controls[idx].x + 1 + int(controls[idx].w / 2) - int(controls[idx].labelWidth / 2)), controls[idx].y + 3);
			gfx->print((*getStringCallback)(controls[idx].labelId));
			break;
		case controlTypes::BIG_BUTTON:
			gfx->setTextSize(2);
			gfx->setCursor((controls[idx].x + 1 + int(controls[idx].w / 2) - int(controls[idx].labelWidth / 2)), controls[idx].y + 3);
			gfx->print((*getStringCallback)(controls[idx].labelId));
			gfx->setTextSize(1);
			break;
	}
}

void page::RefreshContent(byte idx)
{
	int color = ST7735_BLACK;
	int fcolor = ST7735_GREEN;

	// clean the inside of the rectangle
	gfx->fillRect(controls[idx].x + 1, controls[idx].y + 1, controls[idx].w - 2, controls[idx].h - 2, color);
	gfx->setTextColor(fcolor);

	if (controls[idx].type == controlTypes::INPUT_FIELD_INT || controls[idx].type == controlTypes::INPUT_FIELD_DOUBLE || controls[idx].type == controlTypes::LABEL_FIELD) {
		gfx->setCursor(controls[idx].x + 3, controls[idx].y + 3);
		PrintValue(idx);
	}

	if (controls[idx].type == controlTypes::INPUT_FIELD_INT || controls[idx].type == controlTypes::INPUT_FIELD_DOUBLE) {
		switch (AdjustMultiplier) {
		case 10:
			gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 2, 4, ST7735_YELLOW);
			break;
		case 100:
			gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 2, 4, ST7735_YELLOW);
			gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 6, 4, ST7735_YELLOW);
			break;
		case 1000:
			gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 2, 4, ST7735_YELLOW);
			gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 6, 4, ST7735_YELLOW);
			gfx->drawFastHLine(controls[idx].x + controls[idx].w - 6, controls[idx].y + 10, 4, ST7735_YELLOW);
			break;
		}
	}
}

void page::PrintValue(byte idx)
{
	switch (controls[idx].vt) {
		case valueTypes::P_BYTE:
			gfx->print(*controls[idx].vb);
			break;
		case valueTypes::P_INT:
			if (controls[idx].type == controlTypes::INPUT_FIELD_DOUBLE) gfx->print(float(*controls[idx].vi) / 10, 2);
			else gfx->print(*controls[idx].vi);
			
			break;
		case valueTypes::P_WORD:
			if (controls[idx].type == controlTypes::INPUT_FIELD_DOUBLE) gfx->print((float)(*controls[idx].vw) / 100, 2);
			else gfx->print(*controls[idx].vw);

			break;
		case valueTypes::P_LONG:
			gfx->print(*controls[idx].vl);
			break;
		case valueTypes::P_NONE:
			break;
	}
}

void page::encoderPressed()
{
	if (controls[controlFocus].type == controlTypes::BUTTON || controls[controlFocus].type == controlTypes::BIG_BUTTON) {
		(*buttonHandlerCallback)(controls[controlFocus].code);
	}
	else
	{
		if (controls[controlFocus].type != controlTypes::LABEL_FIELD) {
			if ( controlEditing ) AdjustMultiplier = 1;
			controlEditing = !controlEditing;
			RenderFrame(controlFocus);
			RenderContent(controlFocus);
		}
	}
}

void page::encoderMoved(int amount)
{
	if (controlEditing) {
		AdjustValue(amount);
		RenderContent(controlFocus);
	}
	else
	{
		int newFocus = controlFocus + amount;
		byte unFocus = controlFocus;

		if (newFocus > numControls - 1) {
			controlFocus = 0;
		}
		else if (newFocus < 0) {
			controlFocus = numControls - 1;
		}
		else
		{
			controlFocus = byte(newFocus);
		}
		RenderFrame(unFocus);
		RenderFrame(controlFocus);
	}
}

void page::buttonPressed()
{
	if (controlEditing) {
		if (controls[controlFocus].vt == valueTypes::P_INT || controls[controlFocus].vt == valueTypes::P_LONG || controls[controlFocus].vt == valueTypes::P_WORD) {
			switch (AdjustMultiplier) {
			case 1:
				AdjustMultiplier = 10;
				break;
			case 10:
				AdjustMultiplier = 100;
				break;
			case 100:
				AdjustMultiplier = 1000;
				break;
			default:
				AdjustMultiplier = 1;
			}
		}
		
		RenderContent(controlFocus);
	}
}

void page::AdjustValue(int amount)
{
	switch (controls[controlFocus].vt) {
		case valueTypes::P_BYTE:
			*controls[controlFocus].vb += amount;
			break;
		case valueTypes::P_INT:
			*controls[controlFocus].vi += (amount * AdjustMultiplier);
			break;
		case valueTypes::P_WORD:
			*controls[controlFocus].vw += (amount * AdjustMultiplier);
			break;
		case valueTypes::P_LONG:
			*controls[controlFocus].vl += (amount * AdjustMultiplier);
			break;
		case valueTypes::P_NONE:
			break;
	}
}
