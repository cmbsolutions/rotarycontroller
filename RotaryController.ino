//#define DEBUG

#ifdef DEBUG
#define DEBUG_PRINTLN(x) Serial.println (x)
#define DEBUG_PRINT(x) Serial.print (x)
#else
#define DEBUG_PRINTLN(x)
#define DEBUG_PRINT(x)
#endif

const char string_0[] PROGMEM = "RPM";
const char string_1[] PROGMEM = "Step/Deg.";
const char string_2[] PROGMEM = "Step/Rev.";
const char string_3[] PROGMEM = "Microstep";
const char string_4[] PROGMEM = "Back";
const char string_5[] PROGMEM = "Save";
const char string_6[] PROGMEM = "Wait ms";
const char string_7[] PROGMEM = "Angle";
const char string_8[] PROGMEM = "Settings";
const char string_9[] PROGMEM = "Calibrate";
const char string_10[] PROGMEM = "Current";
const char string_11[] PROGMEM = "Last";
const char string_12[] PROGMEM = "Threshold";
const char string_13[] PROGMEM = "Control";
const char string_14[] PROGMEM = "Runs (0=inf)";
const char string_15[] PROGMEM = "Execute";
const char string_16[] PROGMEM = "Manual ctrl";
const char string_17[] PROGMEM = "Step mode";
const char string_18[] PROGMEM = "Degree mode";
const char string_19[] PROGMEM = "Reset";
const char string_20[] PROGMEM = "Steps";
const char string_21[] PROGMEM = "Ena./Dis.";



const char* const string_table[] PROGMEM = { string_0, string_1, string_2, string_3, string_4, string_5, string_6, string_7, string_8, string_9, string_10, string_11, string_12, string_13, string_14, string_15, string_16, string_17, string_18, string_19, string_20, string_21 };

char buffer[20];

String getFromStringTable(byte idx) {
	if (idx > sizeof(string_table)) {
		return "";
	}
	else
	{
		strcpy_P(buffer, (char*)pgm_read_word(&(string_table[idx])));
		return String(buffer);
	}
}

#include <SPI.h>
#include <EEPROM.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <Bounce2.h>
#include <BasicStepperDriver.h>
#include <elapsedMillis.h>
#include "Rotary.h"

#include "page.h"

/* Pin defines */
#define GFX_SDA_PIN 13
#define GFX_SDL_PIN 11
#define GFX_CS_PIN 10
#define GFX_DC_PIN 8
#define GFX_RST_PIN 9
#define ENCODER_A_PIN 2
#define ENCODER_B_PIN 3
#define ENCODER_KEY_PIN 12
#define BUTTON_A_PIN 5
#define LED_PIN 4
#define STEPPER_STEP_PIN 6 
#define STEPPER_DIRECTION_PIN 7
#define STEPPER_ENABLE_PIN 4
#define IR_RECEIVER A4

struct eepromData {
	byte _rpm; // rpm
	word _sptd; // steps per table degree
	byte _str; // steps per stepper rotation
	byte _microstepping; // microstepping
	byte _irthreshold; // ir sensor trigger diffirence
	byte _irhigh; // ir sensor high value (this will be the reflective part)
	byte _irlow; // ir sensor low value (this is the black line)
	word _pt; // pause timer
};

Adafruit_ST7735 gfx(GFX_CS_PIN, GFX_DC_PIN, GFX_RST_PIN);
Bounce btn1 = Bounce(ENCODER_KEY_PIN, 5);
Bounce btn2 = Bounce(BUTTON_A_PIN, 5);
Rotary enc = Rotary(ENCODER_A_PIN, ENCODER_B_PIN);

/* Encoder variables changed by ISR */
volatile int encPos = 0;
int localEncPos = 0;
int oldEncPos = 0;
int diffEncPos = 0;

/* pages */
// pointer to loaded page
page *currentPage;

// Mainmenu page
page menuPage(&gfx, &getFromStringTable, &buttonHandlers);

// settingspage
page settingsPage(&gfx, &getFromStringTable, &buttonHandlers);
byte rpm = 150;
word stepsPerTableDegree = 33065;
int stepsPerRevolution = 200;
byte microsteps = 8;

// sensor calibration page
page calibratePage(&gfx, &getFromStringTable, &buttonHandlers);
byte irThreshold = 0;
byte irHigh = 0;
byte irLow = 255;

// controlpage
page controlPage(&gfx, &getFromStringTable, &buttonHandlers);
word rotationPauseTime = 2000;
int rotationAngle = 0;
byte rotationCount = 0;
long rotationSteps = 0;
bool rotating = false;
bool paused = false;
elapsedMillis pauseTimer;

// Manual control page
page manualPage(&gfx, &getFromStringTable, &buttonHandlers);
long manualSteps = 0;
int manualAngle = 0;
bool manualS = false;
bool manualA = false;

BasicStepperDriver stepper(stepsPerRevolution, STEPPER_DIRECTION_PIN, STEPPER_STEP_PIN, STEPPER_ENABLE_PIN);

void setup()
{
#ifdef DEBUG
	Serial.begin(115200);
#endif //DEBUG

	/* Init pinmodes */
	pinMode(ENCODER_KEY_PIN, INPUT_PULLUP);
	pinMode(BUTTON_A_PIN, INPUT_PULLUP);
	pinMode(STEPPER_STEP_PIN, OUTPUT);
	pinMode(STEPPER_DIRECTION_PIN, OUTPUT);
	pinMode(STEPPER_ENABLE_PIN, OUTPUT);
	pinMode(LED_PIN, OUTPUT);

	pinMode(ENCODER_A_PIN, INPUT_PULLUP);
	pinMode(ENCODER_B_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(ENCODER_A_PIN), ISR_encoder, CHANGE);
	attachInterrupt(digitalPinToInterrupt(ENCODER_B_PIN), ISR_encoder, CHANGE);

	SPI.begin();
	
	/* Init display */
	gfx.initR(INITR_BLACKTAB);
	gfx.setRotation(3);
	gfx.fillScreen(ST7735_BLACK);
	gfx.setTextWrap(false);
	gfx.setTextColor(ST7735_WHITE);
	gfx.setCursor(50, 50);
	gfx.println(F("Loading..."));
	// load EEPROM Data
	loadFromEEPROM();

	/* init stepper */
	stepper.begin(rpm, microsteps);
	stepper.enable();



	
	/* Setup pages and there controls */
	// menuPage
	menuPage.addControl(8, 10, 8, page::controlTypes::BIG_BUTTON, 0, page::valueTypes::P_NONE, ACTION_SETI);
	menuPage.addControl(8, 36, 9, page::controlTypes::BIG_BUTTON, 0, page::valueTypes::P_NONE, ACTION_CALI);
	menuPage.addControl(8, 62, 13, page::controlTypes::BIG_BUTTON, 0, page::valueTypes::P_NONE, ACTION_CONT);
	menuPage.addControl(8, 88, 16, page::controlTypes::BIG_BUTTON, 0, page::valueTypes::P_NONE, ACTION_MANU);

	// settingsPage
	settingsPage.addControl(100, 8, 0, page::controlTypes::INPUT_FIELD_INT, &rpm, page::valueTypes::P_BYTE);
	settingsPage.addControl(100, 22, 1, page::controlTypes::INPUT_FIELD_DOUBLE, &stepsPerTableDegree, page::valueTypes::P_WORD);
	settingsPage.addControl(100, 36, 2, page::controlTypes::INPUT_FIELD_INT, &stepsPerRevolution, page::valueTypes::P_INT);
	settingsPage.addControl(100, 50, 3, page::controlTypes::INPUT_FIELD_INT, &microsteps, page::valueTypes::P_BYTE);
	settingsPage.addControl(8, 110, 4, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_BACK);
	settingsPage.addControl(100, 110, 5, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_SAVE);

	// calibration page
	calibratePage.addControl(8, 110, 4, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_BACK);
	calibratePage.addControl(100, 110, 5, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_SAVE);

	// controlPage
	controlPage.addControl(100, 8, 6, page::controlTypes::INPUT_FIELD_INT, &rotationPauseTime, page::valueTypes::P_WORD);
	controlPage.addControl(100, 22, 7, page::controlTypes::INPUT_FIELD_INT, &rotationAngle, page::valueTypes::P_INT);
	controlPage.addControl(100, 36, 14, page::controlTypes::INPUT_FIELD_INT, &rotationCount, page::valueTypes::P_BYTE);
	controlPage.addControl(100, 50, 0, page::controlTypes::INPUT_FIELD_INT, &rpm, page::valueTypes::P_BYTE);
	controlPage.addControl(8, 110, 4, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_BACK);
	controlPage.addControl(100, 110, 15, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_EXEC);
	
	// Manual control page
	manualPage.addControl(100, 8, 20, page::controlTypes::LABEL_FIELD, &manualSteps, page::valueTypes::P_LONG);
	manualPage.addControl(100, 22, 7, page::controlTypes::INPUT_FIELD_DOUBLE, &manualAngle, page::valueTypes::P_INT);
	manualPage.addControl(40, 36, 17, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_STEP);
	manualPage.addControl(40, 50, 18, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_DEGR);
	manualPage.addControl(40, 64, 19, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_RST);
	manualPage.addControl(40, 78, 21, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_ENA);
	manualPage.addControl(40, 110, 4, page::controlTypes::BUTTON, 0, page::valueTypes::P_NONE, ACTION_BACK);


	currentPage = &menuPage;

	currentPage->RenderPage();
}

void loop()
{
	// manage buttons
	btn1.update();
	btn2.update();

	if (btn1.fell()) {
		currentPage->encoderPressed();
	}

	if (btn2.fell()) {
		if (stepper.getCurrentState() != BasicStepperDriver::STOPPED || paused || manualS || manualA) {
			stepper.stop();
			rotating = false;
			paused = false;
			manualS = false;
			manualA = false;
		}
		else
		{
			currentPage->buttonPressed();
		}
	}

	// manage encoder
	localEncPos = encPos;

	if (localEncPos != oldEncPos) {
		diffEncPos = localEncPos - oldEncPos;
		oldEncPos = localEncPos;

		if (diffEncPos != 0) {
			if (!manualS && !manualA) {
				currentPage->encoderMoved(diffEncPos);
			}
			else if (manualS) {
				stepperManual(diffEncPos);
				currentPage->RefreshContent(0);
			}
			else if (manualA) {
				stepperManual(diffEncPos);
				currentPage->RefreshContent(1);
			}
		}
	}

	stepper.nextAction();

	// manage stepper
	if (rotating) {
		if (stepper.getCurrentState() == BasicStepperDriver::STOPPED) {

			if (rotationCount > 1) {
				rotationCount--;
				currentPage->RefreshContent(2);
			}
			
			if ( rotationCount == 1 ) {
				paused = false;
				rotating = false;
			}
			else
			{
				paused = true;
				pauseTimer = 0;
				rotating = false;
			}
		}
	}

	if (paused) {
		if (pauseTimer > rotationPauseTime) {
			runStepper(false);
			paused = false;
		}
	}
}

/* Button callbacks from all pages */
void buttonHandlers(byte code) {
	switch (code) {
		case ACTION_BACK:
			currentPage = &menuPage;
			currentPage->RenderPage();
			break;
		case ACTION_SETI:
			currentPage = &settingsPage;
			currentPage->RenderPage();
			break;
		case ACTION_CALI:
			currentPage = &calibratePage;
			currentPage->RenderPage();
			break;
		case ACTION_SAVE:
			storeToEEPROM();
			gfx.fillRect(60, 120, 40, 8, ST7735_BLACK);
			gfx.setTextColor(ST7735_MAGENTA);
			gfx.setCursor(60, 120);
			gfx.print("Saved!");
			break;
		case ACTION_CONT:
			currentPage = &controlPage;
			currentPage->RenderPage();
			break;
		case ACTION_EXEC:
			runStepper(true);
			break;
		case ACTION_MANU:
			currentPage = &manualPage;
			currentPage->RenderPage();
			break;
		case ACTION_STEP:
			manualS = true;
			manualA = false;

			break;
		case ACTION_DEGR:
			manualS = false;
			manualA = true;

			break;
		case ACTION_ENA:
			if (digitalRead(STEPPER_ENABLE_PIN) == HIGH) {
				stepper.enable();
			}
			else
			{
				stepper.disable();
			}
			break;
		case ACTION_RST:
			manualSteps = 0;
			manualAngle = 0;
			currentPage->RefreshContent(0);
			currentPage->RefreshContent(1);
			break;
	}
}

/* stepper control code */
void runStepper(bool recalc) {
	if (stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
		if (recalc) {
			float tmp = float(stepsPerTableDegree) / 100;
			rotationSteps = long(tmp * rotationAngle);
			stepper.setRPM(rpm);
		}
		stepper.startMove(rotationSteps);
		rotating = true;
	}
}

void stepperManual(int amount) {

	if (stepper.getCurrentState() == BasicStepperDriver::STOPPED) {
		if (manualS) {
			manualSteps += amount;
			stepper.move(amount);
		}

		if (manualA) {
			manualAngle += amount;
			float tmp = float(stepsPerTableDegree) / 100;
			rotationSteps = long(tmp * (float(amount) / 10));
			stepper.move(rotationSteps);
		}
	}
}

/* EEPROM code (saving and restoring etc) */
void storeToEEPROM() {
	eepromData _saveData = {
		rpm,
		stepsPerTableDegree,
		byte(stepsPerRevolution),
		microsteps,
		irThreshold,
		irHigh,
		irLow,
		rotationPauseTime
	};

	EEPROM.put(0, _saveData);
}

void loadFromEEPROM() {
	eepromData _loadData;

	EEPROM.get(0, _loadData);

	rpm = _loadData._rpm;
	stepsPerTableDegree = _loadData._sptd;
	stepsPerRevolution = _loadData._str;
	microsteps = _loadData._microstepping;
	irThreshold = _loadData._irthreshold;
	irHigh = _loadData._irhigh;
	irLow = _loadData._irlow;
	rotationPauseTime = _loadData._pt;
}

/* ISR Code */
void ISR_encoder() {
	unsigned char result = enc.process();
	if (result == DIR_CW) {
		encPos--;
	}
	else if (result == DIR_CCW) {
		encPos++;
	}
}