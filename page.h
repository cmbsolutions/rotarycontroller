// page.h

#ifndef _page_h
#define _page_h

// colors for adafruit gfx
#define ST7735_OCEANBLUE 0x0336
#define ST7735_DEEPBLUE 0x1336

// actions used in the callbacks of buttons
#define ACTION_BACK 0 // generic back button to mainmenu
#define ACTION_SAVE 1 // save of settings
#define ACTION_EXEC 2 // run controller
#define ACTION_CALI 3 // calibrate screen
#define ACTION_SETI 4 // settingsscreen
#define ACTION_CONT 5 // controlscreen
#define ACTION_MANU 6 // Manual control screen
#define ACTION_STEP 7 // Manual step mode
#define ACTION_DEGR 8 // manual degree mode
#define ACTION_RST 9 // manual reset counters
#define ACTION_ENA 10 // enable/disable stepper

#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>

typedef String(*getString)(byte idx);
typedef void(*buttonHandler)(byte code);

class page {
public:

	static const byte MAX_CONTROLS = 7;

	enum controlTypes { INPUT_FIELD_INT, INPUT_FIELD_DOUBLE, BUTTON, BIG_BUTTON, LABEL_FIELD };
	enum valueTypes { P_NONE, P_BYTE, P_INT, P_WORD, P_LONG };

	struct control {
		byte x;
		byte y;
		byte w;
		byte h;
		byte labelId;
		byte labelWidth;
		controlTypes type;
		valueTypes vt;
		byte code;
		byte *vb;
		int *vi;
		word *vw;
		long *vl;
	};

	page() {}

	page(Adafruit_GFX *gfx, getString f, buttonHandler bh);

	bool addControl(byte x, byte y, byte l, controlTypes t, void* d, valueTypes v, byte c = 0);
	
	bool isFull();

	bool isEmpty();

	byte getNumControls();

	void RenderPage();

	void init();

	void RenderFrame(byte idx);
	void RenderContent(byte idx);
	void RefreshContent(byte idx);

	void PrintValue(byte idx);

	void encoderPressed();

	void encoderMoved(int amount);

	void buttonPressed();

protected:
	Adafruit_GFX *gfx;
	getString getStringCallback;
	buttonHandler buttonHandlerCallback;

	control controls[MAX_CONTROLS];
	byte numControls;
	byte controlsHead, controlsTail;

	byte controlFocus = 0;
	bool controlEditing = false;

	void AdjustValue(int amount);
	int AdjustMultiplier = 1;
};


#endif

